import os,sys
import json
import pathlib
#import glob
from matplotlib import pyplot as plt
import numpy as np
from scipy.optimize import curve_fit




def Run(*args):
    keep_open=True
    def hyp(x, b):
        return a * np.sqrt(1+(x-loc)**2/b**2) 
    
    for files in args:

        folder,filename=os.path.split(files)
        fileroot,ext=os.path.splitext(files)
        os.chdir(folder)
        print('Filename:   ', filename)
        with open(os.path.join(folder,files),'r') as fp:
        #with open(files,'r') as fp:
            data = json.load(fp)

        time_stamp=data['Timestamp']
        print('timestamp ', str(time_stamp))

        measures=data['MeasurePoints']
        pos=np.array([m['Position'] for m in measures])
        val=np.array([m['Value'] for m in measures])
        error=[m['Error'] for m in measures]
         
        intersect=data['Intersections']
        
        meth=data['Fitting']
        print('Fit method: ', meth)
        
        if meth=='PARABOLIC': 
            hyploc=intersect['QuadraticMinimum']['Position']
            hypval=intersect['QuadraticMinimum']['Value']
            
        elif meth=='HYPERBOLIC': 
            hyploc=intersect['HyperbolicMinimum']['Position']
            hypval=intersect['HyperbolicMinimum']['Value']
            
            
        print('Minloc (X): ', str(hyploc))
        print('Minval (Y): ', str(hypval))
        
          
        b0=np.sqrt((pos[-1]-hyploc)**2/((val[-1]/hypval)**2-1))
        a=hypval
        loc=hyploc
        popt, pcov = curve_fit(hyp, pos, val,p0=[b0])
        xref=np.linspace(min(pos),max(pos),num=200)
        yhyp=hyp(xref,  popt[0])

        fig, ax = plt.subplots(1, 1, figsize=(5, 5))
        #color = 'tab:blue'
        ax.set_title(filename.replace(".json", ""))
        ax.set_xlabel('Focuser Position')
        ax.set_ylabel('HFR')
        ax.scatter(pos,val,marker='o')
        color = 'tab:red'
        ax.plot(xref,yhyp, c=color, linestyle='dashed')
        ax.scatter(hyploc,hypval,marker='*',c=color)



        #plt.show()
        plt.savefig(fileroot+'.png')
        print('Out file   :',filename.replace("json", "png"))
        print(' ')
        #plt.show()
        a=0
        
    if keep_open:
        print('Press Enter to close')
        input()


if __name__ == "__main__":
    Run(*sys.argv[1:])
    