# AF_Curve_NINA

A short py script to get the Autofocus Fitted curve from json file created by NINA

# Getting started

Just save the py file where you want on your computer.
Supported input files are the json files generated by NINA, my favorite astronomical assistant... 

# Usage
Drop the selected json file(s) (multiple selection is allowed) upon the .py file in order to "open them with..."
PNG images are immediatly created just beside the origin json file, with matching file names.

